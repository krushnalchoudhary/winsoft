import java.util.Scanner;
public class DuplicateCharacters {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String str = sc.next();

        str = str.toLowerCase();

        for (int i = 0; i < str.length(); i++) {
            char currentChar = str.charAt(i);
            
            if (currentChar >= 'a' && currentChar <= 'z') {
                boolean isDuplicate = false;
                for (int j = i + 1; j < str.length(); j++) {
                    if (str.charAt(j) == currentChar) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (isDuplicate) {
                    System.out.println(currentChar);
                }
            }
        }
    }
}
