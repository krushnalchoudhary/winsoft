import java.util.HashMap;
import java.util.Scanner;

public class WordCounter {
    public static void main(String[] args) {

        System.out.println("Enter String");
        Scanner sc = new Scanner(System.in);

        String text = sc.next();

        //String text = "Hello world hello Java world";

        
        String[] words = text.split("\\s+");

        HashMap<String, Integer> wordCountMap = new HashMap<>();

        for (String word : words) {
            if (wordCountMap.containsKey(word)) {
                wordCountMap.put(word, wordCountMap.get(word) + 1);
            } else { 
                wordCountMap.put(word, 1);
            }
        }

        System.out.println("Word Counts:");
        for (String word : wordCountMap.keySet()) {
            System.out.println(word + ": " + wordCountMap.get(word));
        }
    }
}
